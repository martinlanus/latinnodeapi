const express = require('express')
const app = express();
const mysql = require('mysql');
const dotenv = require('dotenv');
const path = require('path');
const fs = require('fs');
const https = require('https');
var cors = require('cors');

//const util = require('util')
app.use(cors());

dotenv.config({ path: path.join(__dirname, './process.env') })

const db = mysql.createConnection({
    host     : process.env.db_host,
    user     : process.env.db_user,
    password : process.env.db_password,
    database : process.env.db_database,
    multipleStatements: true,  
    charset : 'utf8mb4'
});

db.connect(function(error){
    if (error) {
        throw error;
    } else {
        console.log('Conected to DB');
    }   
});

getRanking = (instr) =>{
    return new Promise((resolve, reject)=>{
        db.query(instr,  (error, elements)=>{
            if(error){
                return reject(error);
            }
            return resolve(elements);
        });
    });
};
 
 


app.get('/', (req, res) => {
    res.send('LatinNode API')
});

app.get('/api/validatorsold/:scope', async (req, res) => {
    let scope = req.params.scope;
    console.log(scope);
    let queryString = '';
    switch (scope) {
        case 'last':
            queryString = `SELECT
                                val.vote_pk AS Public_Key,
                                valinfo.name AS Name,
                                CONCAT(ROUND((val.apy*100),1),' %') AS APY,
                                ROUND(val.active_stake/1000000000,0) AS Active_Stake,
                                val.commission AS Commission,
                                valinfo.website AS Website,
                                valinfo.details AS Details
                            FROM validators AS val
                            LEFT JOIN validator_info AS valinfo ON val.identity_pk = valinfo.identity_pk
                            WHERE val.rewards_epoch IN (SELECT MAX(validators.rewards_epoch) FROM validators) AND val.epoch = val.rewards_epoch +1 
                            ORDER BY APY DESC LIMIT 100;`;
            break;
        case 'prev3':
            queryString =  `SELECT
                                val.vote_pk AS Public_Key,
                                valinfo.name AS Name,
                                CONCAT(ROUND(((SUM(val.apy)/3)*100),1),' %') AS APY,
                                ROUND((SUM(val.active_stake)/3)/1000000000,0) AS Active_Stake,
                                SUM(val.commission)/3 AS Commission,
                                valinfo.website AS Website,
                                valinfo.details AS Details
                            FROM validators AS val
                            LEFT JOIN validator_info AS valinfo ON val.identity_pk = valinfo.identity_pk
                            WHERE val.rewards_epoch >= (SELECT MAX(validators.rewards_epoch) FROM validators)-2 AND val.epoch = val.rewards_epoch +1 
                            GROUP BY Public_Key, Name, Website, Details
                            ORDER BY APY DESC LIMIT 100`;  
            break;
        case 'prev10':
            queryString =  `SELECT
                                val.vote_pk AS Public_Key,
                                valinfo.name AS Name,
                                CONCAT(ROUND(((SUM(val.apy)/10)*100),1),' %') AS APY,
                                ROUND((SUM(val.active_stake)/10)/1000000000,0) AS Active_Stake,
                                SUM(val.commission)/10 AS Commission,
                                valinfo.website AS Website,
                                valinfo.details AS Details
                            FROM validators AS val
                            LEFT JOIN validator_info AS valinfo ON val.identity_pk = valinfo.identity_pk
                            WHERE val.rewards_epoch >= (SELECT MAX(validators.rewards_epoch) FROM validators)-9 AND val.epoch = val.rewards_epoch +1 
                            GROUP BY Public_Key, Name, Website, Details
                            ORDER BY APY DESC LIMIT 100`; 
    } 
    console.log(queryString)
    db.query(queryString, (error,fields)=> {
        if(error) {
            console.log('db error');
            throw error;
        } else {
            res.send(fields);
            console.log('response sent '+JSON.stringify(fields)+'/n');
        }
    })
});

app.get('/api/validators/:scope', async (req, res) => {
    let scope = req.params.scope;
    console.log(scope);
    let queryString = '';
    switch (scope) {
        case 'last':
            queryString =  `SELECT
                                val.vote_pk AS Public_Key,
                                valinfo.name AS Name,
                                CONCAT(ROUND((val.apy*100),1),' %') AS APY,
                                ROUND(val.active_stake/1000000000,0) AS Active_Stake,
                                val.commission AS Commission,
                                valinfo.website AS Website,
                                valinfo.details AS Details
                            FROM validators AS val
                            LEFT JOIN validator_info AS valinfo ON val.identity_pk = valinfo.identity_pk
                            WHERE val.rewards_epoch IN (SELECT MAX(validators.rewards_epoch) FROM validators) AND val.epoch = val.rewards_epoch +1 
                            ORDER BY APY DESC LIMIT 100;`;
                            console.log("voy al break #1");
            break;
        case 'prev3':
            queryString =  `SELECT
                                val.vote_pk AS Public_Key,
                                valinfo.name AS Name,
                                CONCAT(ROUND(((SUM(val.apy)/3)*100),1),' %') AS APY,
                                ROUND((SUM(val.active_stake)/3)/1000000000,0) AS Active_Stake,
                                SUM(val.commission)/3 AS Commission,
                                valinfo.website AS Website,
                                valinfo.details AS Details
                            FROM validators AS val
                            LEFT JOIN validator_info AS valinfo ON val.identity_pk = valinfo.identity_pk
                            WHERE val.rewards_epoch >= (SELECT MAX(validators.rewards_epoch) FROM validators)-2 AND val.epoch = val.rewards_epoch +1 
                            GROUP BY Public_Key, Name, Website, Details
                            ORDER BY APY DESC LIMIT 100;`;  
                            console.log("voy al break #2");

            break;
        case 'prev10':
            queryString =  `SELECT
                                val.vote_pk AS Public_Key,
                                valinfo.name AS Name,
                                CONCAT(ROUND(((SUM(val.apy)/10)*100),1),' %') AS APY,
                                ROUND((SUM(val.active_stake)/10)/1000000000,0) AS Active_Stake,
                                SUM(val.commission)/10 AS Commission,
                                valinfo.website AS Website,
                                valinfo.details AS Details
                            FROM validators AS val
                            LEFT JOIN validator_info AS valinfo ON val.identity_pk = valinfo.identity_pk
                            WHERE val.rewards_epoch >= (SELECT MAX(validators.rewards_epoch) FROM validators)-9 AND val.epoch = val.rewards_epoch +1 
                            GROUP BY Public_Key, Name, Website, Details
                            ORDER BY APY DESC LIMIT 100;`; 
                            console.log("LLegue al case  #3");

    } 
    try {
        console.log(queryString);
        const resultElements = await getRanking(queryString);
        console.log(resultElements);
        res.status(200).json(resultElements); // send a json response
    } catch(e) {
        console.log(e); // console log the error so we can see it in the console
        res.sendStatus(500);
    }
});

app.listen(8000, () => {
    console.log('Example app listening on port 8000!')
});

https
  .createServer(
    {
      key: fs.readFileSync('/etc/letsencrypt/live/latinnode.com/privkey.pem'),
      cert: fs.readFileSync('/etc/letsencrypt/live/latinnode.com/fullchain.pem'),
      ca: fs.readFileSync('/etc/letsencrypt/live/latinnode.com/fullchain.pem'),
    },
    app
  )
  .listen(8443, () => {
    console.log('Listening...')
  })
